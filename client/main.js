import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

import { SankiApp } from './sankhya.js';

Template.sankhya.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.sanki = new ReactiveVar();
});

Template.sankhya.helpers({
  sanki: function() {
    return Session.get('sanki');
  }
});

Template.sankhya.events({
  'keyup .mainfull'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const text = target.value;

    const res = SankiApp(text);

    return Session.set("sanki", res);

  },
});

//Fr7
if (Meteor.isClient) {
  // Initialize app
  var myApp = new Framework7();

  // If we need to use custom DOM library, let's save it to $$ variable:
  var $$ = Dom7;

}
